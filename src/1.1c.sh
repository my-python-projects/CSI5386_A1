#!/usr/bin/env bash

sort ../Resources/microblog2011_tokenized.txt | uniq -c | sort -nr > ../Resources/Tokens.txt
head -n100 ../Resources/Tokens.txt > ../Resources/top100Tokens.txt
