from nltk.tokenize import TweetTokenizer
from nltk import pos_tag

with open('../Resources/POS_tagger_input.txt', 'r') as input_file:
    for tweet in input_file:
        tokenizer = TweetTokenizer()
        tagged_tokens = pos_tag(tokenizer.tokenize(tweet))
        for token in tagged_tokens:
            print("Token")
        print(tagged_tokens)