import string

additional_punctuation = """’“”‘»«─"""

with open('../Resources/Tokens.txt', 'r') as input_file:
    with open('../Resources/Tokens_without_punctuation.txt', 'w') as output_file:
        for count_token in input_file:
            skip_token = False
            for char in count_token:
                # Skip the token if there is punctuation in it.
                if char in string.punctuation or char in additional_punctuation:
                    skip_token = True
            if not skip_token:
                output_file.write(count_token)