import urllib.parse
import nltk

with open('../Resources/microblog2011_formatted.txt', 'w') as formatted_output_file:
    with open('../Resources/microblog2011.txt', 'r') as input_file:
        for tweet in input_file:
            # Remove end of line
            tweet = tweet.strip('\n').lower()

            # Split each elements of the tweet.
            elements = tweet.split(' ')

            # Apply each formatting elements.
            elements = ['REDACTEDURL' if bool(urllib.parse.urlparse(element)[1]) else element for element in elements]

            # Re-form the string as a whole and write it to a file.
            formatted_output_file.write(" ".join(elements))

            # End the tweet with a new line.
            formatted_output_file.write('\n')


with open('../Resources/microblog2011_formatted.txt', 'r') as input_file:
    tokens = nltk.TweetTokenizer().tokenize(input_file.read())
    with open('../Resources/microblog2011_tokenized.txt', 'w') as output_file:
        for token in tokens:
            output_file.write(token)
            output_file.write('\n')
