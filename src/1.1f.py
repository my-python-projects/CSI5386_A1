with open('../Resources/StopWords', 'r') as stopwords_list:
    stopwords = stopwords_list.readlines()
    with open('../Resources/Tokens_without_punctuation.txt', 'r') as words_list:
        with open('../Resources/Tokens_without_punctuation_and_stopword.txt', 'w') as output_file:
            for word in words_list:
                if word.lstrip(' ').split(' ')[1] not in stopwords:
                    output_file.write(word)